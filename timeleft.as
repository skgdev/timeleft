/**
**/

void OnPluginInit()
{
    SetPluginMeta();
    RegisterHooks();
}

void SetPluginMeta()
{
	PluginData::SetVersion( "2.0.0.0" );
	PluginData::SetAuthor( "Chris \"Majorpayne327\"" );
	PluginData::SetName( "Time Left" );
}

void RegisterHooks(){
    Events::Player::OnPlayerConnected.Hook( @SendTimeLeft );
    Events::Rounds::LastRound.Hook( @SendLastRoundMessage );
    Events::Player::PlayerSay.Hook( @TimeLeftCommand );
}

/**
Send the time left on the map to the user that logged in
**/
HookReturnCode SendTimeLeft(CZP_Player@ pPlayer)
{
    Chat.PrintToChatPlayer(pPlayer.opCast(), BuildTimeLeftTxt());
    return HOOK_CONTINUE;
}

string BuildTimeLeftTxt()
{
    float timeLeft = RoundManager.GetTimeLeft();
    string timeLeftTxt = "Time Remaining: " + BuildTimeString(timeLeft);
    return timeLeftTxt;
}

string BuildTimeString(int &in seconds)
{
    int mins = seconds / 60;
    int secs = seconds % 60;
    string time = formatInt(ClampToZero(mins)) + ":" + formatInt(ClampToZero(secs), "0", 2);
    return time;
}

uint ClampToZero(uint num){
    if (num <= 0)
    {
        return 0;
    }
    return num;
}

/**
Send a message to all players that they are on the last round
**/
HookReturnCode SendLastRoundMessage()
{
    const string finalRoundTxt = "This is the final round!!!; " + BuildTimeLeftTxt();
    Chat.PrintToChat(all, finalRoundTxt);
    return HOOK_CONTINUE;
}

/**
Handle the !timeleft command
**/
HookReturnCode TimeLeftCommand(CZP_Player@ pPlayer, CASCommand@ pArgs)
{
    if ( pArgs is null )
        return HOOK_CONTINUE;
	
	string arg1 = pArgs.Arg( 1 );

    if (Utils.StrEql(arg1, "!timeleft"))
    {
        Chat.PrintToChatPlayer(pPlayer.opCast(), BuildTimeLeftTxt());
        return HOOK_HANDLED;
    }

    return HOOK_CONTINUE;
}
